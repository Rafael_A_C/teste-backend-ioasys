/**
 *  Dependencies
 **/
const BaseController = require('./base_controller')

class HealthController extends BaseController {
  handler (req, res) {
    return res.status(200).json()
  }
}

module.exports = HealthController
