/**
 *  Dependencies
 **/
const BaseController = require('./base_controller')

class SessionController extends BaseController {
  constructor (sessionService) {
    super()
    this._sessionService = sessionService
  }

  async handler (req, res) {
    const { email, password } = req.body

    /** Validations */
    if (!email || !password) {
      return res.status(400).json({ error: 404, message: 'Body required email and password fields' })
    }
    if (typeof password !== 'string' || password.length < 6) {
      return res.status(400).json({ error: 404, message: 'Password Field want to be a string and > 6' })
    }
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if (typeof email !== 'string' || !re.test(email)) {
      return res.status(400).json({ error: 404, message: 'Email Field want to be a string and email' })
    }
    /** */

    const result = await this._sessionService.session({ email, password })

    return res.status(200).json()
  }
}

module.exports = SessionController
