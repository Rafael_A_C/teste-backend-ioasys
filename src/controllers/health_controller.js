/**
 *  Dependencies
 **/
const BaseController = require('./base_controller')

class HealthController extends BaseController {
  handler (req, res) {
    const now = new Date()

    return res.status(200).json({ msg: 'server running', starTupTme: req.starTupTime, now })
  }
}

module.exports = HealthController
