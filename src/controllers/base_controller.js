class BaseController {
  constructor () {
    if (this.constructor === BaseController) {
      throw new Error("Abstract classes can't be instantiated.")
    }
  }

  handler (req, res) {
    throw new Error("Method 'handle(req, res)' must be implemented.")
  }
}

module.exports = BaseController
