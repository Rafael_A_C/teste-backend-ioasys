module.exports = function RoleBasedAccessControl (permission) {
  return async function (req, res, next) {
    const { auth } = req

    if (!auth) {
      return res.status(403).json({ error: 403, msg: 'role do not provider' })
    }

    const { permissionId } = auth
    if (!permissionId) {
      return res.status(403).json({ error: 403, msg: 'role do not provider' })
    }

    switch (permission) {
      case 'can:user':
        if (permissionId === 1 || permissionId === 2 || permissionId === 3) {
          return next()
        }
        return res
          .status(403)
          .json({ error: 403, msg: 'role do not authorized' })

      case 'can:admin':
        if (permissionId === 1 || permissionId === 2) {
          return next()
        }
        return res
          .status(403)
          .json({ error: 403, msg: 'role do not authorized' })

      case 'can:sysops':
        if (permissionId === 1) {
          return next()
        }
        return res
          .status(403)
          .json({ error: 403, msg: 'role do not authorized' })

      default:
        return res
          .status(403)
          .json({ error: 403, msg: 'role do not authorized' })
    }
  }
}
