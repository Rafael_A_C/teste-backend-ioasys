const JWT = require('jsonwebtoken')
const { promisify } = require('util')

async function JWTDecoded (token, secret) {
  try {
    return await promisify(JWT.verify)(token, secret)
  } catch (err) {
    return false
  }
}

module.exports = async (req, res, next) => {
  const authHeader = req.headers.authorization

  if (!authHeader) {
    return res.status(403).json({ error: 403, msg: 'Do not provider token' })
  }
  const [, token] = authHeader.split(' ')

  const auth = await JWTDecoded(token, process.env.AUTH_SECRET_TOKEN)

  if (!auth) {
    return res.status(403).json({ error: 403, msg: 'invalid credential' })
  }

  req.auth = {
    userId: auth.id,
    permissionId: auth.x
  }

  return next()
}
