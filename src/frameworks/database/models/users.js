module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define(
    'users',
    {
      permissionId: {
        type: DataTypes.STRING,
        field: 'permission_id'
      },
      name: {
        type: DataTypes.STRING
      },
      email: {
        type: DataTypes.STRING
      },
      password: {
        type: DataTypes.STRING
      }
    }

  )

  Users.associate = function (models) {
    Users.belongsTo(models.Permissions, {
      foreignKey: 'permission_id',
      as: 'permission'
    })

    Users.hasOne(models.Votes, {
      foreignKey: 'userId',
      as: 'votes'
    })
  }

  return Users
}
