module.exports = (sequelize, DataTypes) => {
  const Movies = sequelize.define(
    'movies',
    {
      name: {
        type: DataTypes.STRING
      },
      director: {
        type: DataTypes.STRING
      },
      actors: {
        type: DataTypes.STRING
      },
      genre: {
        type: DataTypes.STRING
      },
      releaseDate: {
        type: DataTypes.DATE,
        filed: 'release_date'
      }
    })

  Movies.associate = function (models) {
    Movies.hasOne(models.Users, {
      foreignKey: 'permissionId',
      as: 'permission'
    })
  }

  return Movies
}
