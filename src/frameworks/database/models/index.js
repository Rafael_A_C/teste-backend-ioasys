const Sequelize = require('sequelize')

/**
 * Models
 */

const Permissions = require('./permissions')
const Users = require('./users')
const Movies = require('./movies')
const Votes = require('./votes')

const config = require('../config')

const sequelize = new Sequelize(
  config.database,
  config.username,
  config.password,
  config
)

const models = {

  Permissions: Permissions(sequelize, Sequelize),
  Users: Users(sequelize, Sequelize),
  Movies: Movies(sequelize, Sequelize),
  Votes: Votes(sequelize, Sequelize)

}

Object.keys(models).forEach((modelName) => {
  if (models[modelName].associate) {
    models[modelName].associate(models)
  }
})

models.sequelize = sequelize
models.Sequelize = Sequelize

module.exports = models
