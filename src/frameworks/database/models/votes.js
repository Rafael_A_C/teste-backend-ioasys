module.exports = (sequelize, DataTypes) => {
  const Votes = sequelize.define(
    'votes',
    {
      userId: {
        type: DataTypes.INTEGER,
        field: 'user_id'
      },
      movieId: {
        type: DataTypes.INTEGER,
        field: 'movie_id'
      },
      vote: {
        type: DataTypes.FLOAT
      }
    })

  Votes.associate = function (models) {
    Votes.belongsTo(models.Users, {
      foreignKey: 'user_id',
      as: 'user'
    })

    Votes.belongsTo(models.Users, {
      foreignKey: 'movie_id',
      as: 'movie'
    })
  }

  return Votes
}
