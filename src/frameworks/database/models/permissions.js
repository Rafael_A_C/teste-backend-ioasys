module.exports = (sequelize, DataTypes) => {
  const Permissions = sequelize.define(
    'permissions',
    {
      name: {
        type: DataTypes.STRING
      },
      description: {
        type: DataTypes.STRING
      }
    })

  Permissions.associate = function (models) {
    Permissions.hasOne(models.Users, {
      foreignKey: 'permissionId',
      as: 'permission'
    })
  }

  return Permissions
}
