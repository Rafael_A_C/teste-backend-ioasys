const { Permissions } = require('../models')

// run this like: yarn sequelize db:seed --seed 1608223690782_create_permissions
module.exports = {
  up: async () => {
    const permissions = await Permissions.bulkCreate([
      {
        name: 'sysop',
        description: 'System Operator'
      },
      {
        name: 'admin',
        description: 'System Administrator'
      },
      {
        name: 'users',
        description: 'Normal User'
      }
    ])
    return permissions
  },
  down: async () => {

  }
}
