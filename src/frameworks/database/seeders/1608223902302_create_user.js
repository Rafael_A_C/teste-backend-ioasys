const { Users } = require('../models')
const { hash } = require('bcryptjs')

// run this like: yarn sequelize db:seed --seed 1608223902302_create_user
module.exports = {
  up: async () => {
    const passwordHash = await hash('123456', 8)

    const user = await Users.create({
      permissionId: 1,
      name: 'System Operator',
      email: 'sysop@email.com',
      password: passwordHash
    })
    return user
  },
  down: async () => {

  }
}
