const { Movies } = require('../models')

// run this like: yarn sequelize db:seed --seed 1608224072911_create_movies
module.exports = {
  up: async () => {
    const movies = await Movies.bulkCreate([
      {
        name: 'The Seven Samurai',
        director: 'Akira Kurosawa',
        actors: 'Toshiro Mifune, Takashi Shimura',
        genre: 'science fiction',
        releaseDate: new Date('2014', '0', '1')
      },
      {
        name: 'Bonnie and Clyde',
        director: 'Arthur Penn',
        actors: 'Warren Beatty, Faye Dunaway',
        genre: 'classic',
        releaseDate: new Date('2014', '0', '1')
      },
      {
        name: 'Reservoir Dogs',
        director: 'Quentin Tarantino',
        actors: 'Harvey Keitel, Tim Roth, Chris Penn, SteveBuscemi, Lawrence Tierney, Michael Madsen',
        genre: 'action',
        releaseDate: new Date('2014', '0', '1')
      }
    ])
    return movies
  },
  down: async () => {

  }
}
