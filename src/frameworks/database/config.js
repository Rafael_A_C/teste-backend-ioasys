require('dotenv').config({
  path: `.env.${process.env.NODE_ENV || 'development'}`
})

module.exports = {
  dialect: process.env.DB_DIALECT,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  username: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DATABASE,
  logging: (logging) => {
    if (process.env.NODE_ENV === 'development') console.log(logging)
  },
  define: {
    timestamps: true, // created_at and updated_at
    underscored: true, // to use underscored format on columns and table names
    underscoredAll: true, // to use underscored format on columns and table names
    freezeTableName: true, // to sequelize do not change ta table names
    paranoid: true // to use soft delete
  }
}
