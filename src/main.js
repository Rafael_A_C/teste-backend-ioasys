/**
 *  Dependencies
 **/
const dotEnv = require('dotenv')
dotEnv.config({ path: process.env.NODE_ENV === 'production' ? '.env.production' : '.env.development' })

const Server = require('./server')
const HealthController = require('./controllers/health_controller')

function bootstrap () {
  const server = new Server()
  server.init()

  const apiHealthController = new HealthController()
  server.registerRoute('get', '/', apiHealthController)

  server.listening()
}
bootstrap()
/** */
