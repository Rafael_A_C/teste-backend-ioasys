/**
 *  Dependencies
 **/
const express = require('express')
const cors = require('cors')
const helmet = require('helmet')
const rateLimit = require('express-rate-limit')
const BaseController = require('./controllers/base_controller')
/** */

/**
 * SERVER
 */
class Server {
  constructor () {
    this.server = express()
    this.starTupTime = new Date()
  }

  init () {
    this.server.use(cors())
    this.server.use(helmet())
    this.server.use(express.json())
    const limiter = rateLimit({
      windowMs: 10 * 60 * 1000, // 10 minutes
      max: 1000 // limit each IP to 1000 requests per windowMs
    })
    this.server.use(limiter)
    this.server.use((req, _, next) => {
      req.starTupTime = this.starTupTime
      return next()
    })
  }

  registerRoute (method = 'get', route, controller) {
    if (
      method !== 'get' && method !== 'post' && method !== 'put' && method !== 'delete'
    ) {
      return {
        left: new Error(`REGISTER ROUTE - METHOD: ${method} NOT ALLOWED`),
        right: false
      }
    }
    if (!route && typeof route !== 'string') {
      return {
        left: new Error('REGISTER ROUTE - STRING ROUTE IS REQUIRED'),
        right: false
      }
    }
    if (!(controller instanceof BaseController)) {
      return {
        left: new Error('REGISTER ROUTE - CONTROLLER MUST BE BASE CONTROLLER EXTENSION'),
        right: false
      }
    }
    this.server[method](route, controller.handler.bind(controller))
  }

  listening () {
    const PORT = 8888
    this.server.listen(PORT, () => console.log(`Server running and listening on: 127.0.0.1:${PORT}`))
  }
}

module.exports = Server
