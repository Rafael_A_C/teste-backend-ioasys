class SessionService {
  constructor (userRepository) {
    this._userRepository = userRepository
  }

  async session ({ email, password }) {
    const user = this._userRepository.findByEmail(email)

    if (!user) {
      return { statusCode: 409, body: { error: 409, message: 'User Not Found' } }
    }
  }
}
